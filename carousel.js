document.addEventListener("DOMContentLoaded", (event) => {
  console.log("DOM fully loaded and parsed");
  const swiper = new Swiper(".swiper-container", {
    slidesPerView: 1,
    spaceBetween: 0,
    loop:true,
    speed: 600,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
  });

  function adjustSwiperContainer() {
    var scrollbarWidth =
      window.innerWidth - document.documentElement.clientWidth;
    document.documentElement.style.setProperty(
      "--scrollbarWidth",
      `${scrollbarWidth}px`
    );

    var swiperContainer = document.querySelector(".swiper-container");
    if (swiperContainer) {
      swiperContainer.style.width = `${document.documentElement.clientWidth}px`;
      swiperContainer.style.position = "absolute";
    }
  }

  adjustSwiperContainer(); // Call the function initially to set the styles
  window.addEventListener("resize", adjustSwiperContainer);
});
